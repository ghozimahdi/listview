package com.blank.root.listviewbaru;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private int[] image = {R.drawable.aaa,R.drawable.adsf,R.drawable.fs2185e,R.drawable.asdf,R.drawable.ggg,R.drawable.images};
    private String[] text = {"Baju programmer","Jaket","Baju Kokoh","Baju Google","Baju Rombeng","Baju Batik"};
    private String[] ukuran = {"xl","double xl","l","s","m","l"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter adapter = new adapter(this,text,ukuran,image);
        listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);
    }
}
