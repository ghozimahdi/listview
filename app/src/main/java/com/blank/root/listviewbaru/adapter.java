package com.blank.root.listviewbaru;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by root on 18/07/16.
 */
public class adapter extends ArrayAdapter {
    private Context context;
    private String[] txtukuran;
    private String[] txtnama;
    private int[] img;
    //construktor
    public adapter(Context context,String[] txtnama,String[] txtukuran,int[] img) {
        super(context,R.layout.row_item,txtnama);

        this.context = context;
        this.txtnama = txtnama;
        this.txtukuran = txtukuran;
        this.img = img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_item,null);
        }
        ImageView imageView = (ImageView)view.findViewById(R.id.img);
        imageView.setImageResource(img[position]);
        TextView textView1 = (TextView)view.findViewById(R.id.txt_nama);
        textView1.setText(txtnama[position]);
        TextView textView2 = (TextView)view.findViewById(R.id.txt_ukuran);
        textView2.setText(txtukuran[position]);
        Button button = (Button)view.findViewById(R.id.btn_order);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Order" + txtnama[position], Toast.LENGTH_LONG).show();
            }
        });
        return (view);
    }
}
